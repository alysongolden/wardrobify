from django.db import models
from django.core.validators import URLValidator

# Create your models here.
class BinVO(models.Model):
    closet_name = models.CharField(max_length=200)
    bin_number = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return f'{self.bin_number}'


class Shoes(models.Model):

    manufacturer = models.CharField(max_length=100)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(validators=[URLValidator()])
    bin = models.ForeignKey(
        BinVO,
        related_name='shoes',
        on_delete=models.CASCADE,
    )
    def __str__(self):
        return self.model_name
