from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import BinVO, Shoes


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "bin_number",
        "import_href"
    ]


class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "id",
        "model_name",
        "manufacturer",
        "color",
        "picture_url",
        "bin"
    ]

    encoders = {
        "bin": BinVOEncoder(),
    }

    # def get_extra_data(self, o):
    #     return {"conference": o.conference.name}



class ShoeDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "id",
        "model_name",
        "manufacturer",
        "color",
        "picture_url",
        "bin"  #  Add other fields as needed
    ]

    encoders = {
        "bin": BinVOEncoder(),
    }



@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse(
            shoes,
            encoder= ShoesListEncoder,
            safe=False
        )
    # if request.method == "GET":
    #     shoes = Shoes.objects.all()
    #     return JsonResponse(
    #         {"shoes": shoes},
    #         safe=False)
    else:
        content = json.loads(request.body)

        # Get the Bin object and put it in the content dict

        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin id"},
                status=400,
            )

        shoes = Shoes.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoes(request, pk):
    """
    Single-object API for the Bin resource.

    GET:
    Returns the information for a Bin resource based
    on the value of pk
    {
        "id": database id for the bin,
        "closet_name": bin's closet name,
        "bin_number": the number of the bin,
        "bin_size": the size of the bin,
        "href": URL to the bin,
    }

    PUT:
    Updates the information for a Bin resource based
    on the value of the pk
    {
        "closet_name": bin's closet name,
        "bin_number": the number of the bin,
        "bin_size": the size of the bin,
    }

    DELETE:
    Removes the bin resource from the application
    """
    if request.method == "GET":
        try:
            shoes = Shoes.objects.get(id=pk)
            return JsonResponse(
                shoes,
                encoder= ShoeDetailEncoder,
                safe=False
            )
        except Shoes.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            shoes = Shoes.objects.get(id=pk)
            shoes.delete()
            return JsonResponse(
                shoes,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoes.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            shoes = Shoes.objects.get(id=pk)

            props = [
                "model_name",
                "manufacturer",
                "color",
                "picture_url",
                "bin"
            ]
            for prop in props:
                if prop in content:
                    setattr(shoes, prop, content[prop])
            shoes.save()
            return JsonResponse(
                shoes,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoes.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
