import React, { useEffect, useState } from 'react';

function ShoesForm() {
  const [bin, setBin] = useState([]);

  //Notice that we can condense all formData
  //into one state object
  const [formData, setFormData] = useState({
    manufacturer: '',
    model_name: '',
    color: '',
    picture_url: '',
    bin: '',

  })


  const fetchData = async () => {
    const url = 'http://localhost:8100/api/bins';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setBin(data.bins); // Extract bins
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = 'http://localhost:8080/api/shoes/';


    const fetchConfig = {
      method: "post",
      //Because we are using one formData state object,
      //we can now pass it directly into our request!
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      //The single formData object
      //also allows for easier clearing of data
      setFormData({
        manufacturer: '',
        model_name: '',
        color: '',
        picture_url: '',
        bin: '',
      });
    }
  }

  //Notice that we can also replace multiple form change
  //eventListener functions with one
  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    //We can condense our form data event handling
    //into on function by using the input name to update it

    setFormData({
      //Previous form data is spread (i.e. copied) into our new state object
      ...formData,

      //On top of the that data, we add the currently engaged input key and value
      [inputName]: value
    });
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a new shoe</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">

            <div className="form-floating mb-3">

              <input onChange={handleFormChange} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control" value={formData.model_name} />
              <label htmlFor="model_name">Model Name</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" value={formData.manufacturer} />
              <label htmlFor="manufacturer">Manufacturer</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={formData.color} />
              <label htmlFor="color">Color</label>
            </div>

            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Picture Url" required type="text" name="picture_url" id="picture_url" className="form-control" value={formData.picture_url} />
              <label htmlFor="color">Picture Url</label>
            </div>

            <div className="mb-3">
              <select
                onChange={handleFormChange}
                required
                name="bin"
                id="bin"
                className="form-select"
                // No need for value here if handling selection differently
              >
                <option value="">Choose a closet</option>
                  {bin.map((bin, index) => (
                    <option key={index} value={bin.href}>
                      {bin.closet_name} - {bin.bin_number}
                    </option>
                  ))}
              </select>
            </div>

            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}
export default ShoesForm;
