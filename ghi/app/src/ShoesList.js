import { useEffect, useState } from "react";
import { Link } from 'react-router-dom';
import React from 'react';

function ShoesCard(props) {
  const [deleted, setDeleted] = React.useState(false);

  const shoes = props.shoes;
  console.log(shoes)
  const handleDelete = async () => {
    try {
      const url = `http://localhost:8080/api/shoes/${shoes.id}`;
      const response = await fetch(url, { method: 'DELETE' });

      if (response.ok) {
        console.log("Shoe successfully deleted!");
        // Set the deleted state to true to trigger a re-render
        setDeleted(true);
        // Reload the page
        window.location.reload();
      } else {
        console.error('Delete failed:', response);
      }
    } catch (error) {
      console.error('Error during deletion:', error);
    }
  }

  // Render nothing if the hat is deleted
  if (deleted) {
    return null;
  }


  return (
    <div key={shoes.id} className="card mb-3 shadow p-3">
      <img src={shoes.picture_url} className="card-img-top" />
      <div className="card-body">
        <ul className="list-group list-group-flush">
          <li className="list-group-item">
            Model Name: {shoes.model_name}
          </li>
          <li className="list-group-item">
            Manufacturer: {shoes.manufacturer}
          </li>
          <li className="list-group-item">
            Color: {shoes.color}
          </li>
          <li className="list-group-item">
            Closet: {shoes.bin.closet_name}
          </li>
          <li className="list-group-item">
            Bin: {shoes.bin.bin_number}
          </li>
        </ul>
        <div className="text-center">
          <button onClick={handleDelete} className="btn-outline-secondary">Delete</button>
        </div>
      </div>
    </div>
  );
}

function ShoesColumn(props) {
  return (
    <div className="col">
      {props.list.map((shoes, index) => {
        return <ShoesCard key={index} shoes={shoes} />;
      })}
    </div>
  );
}

const ShoesList = () => {
  const [shoesColumns, setShoesColumns] = useState([[], [], []]);

  const fetchData = async () => {
    const url = "http://localhost:8080/api/shoes/";
    try {
      const response = await fetch(url);

      const columns = [[], [], []];
      if (response.ok) {
        const data = await response.json();

        let index = 0;
        for (const shoes of data) {
          columns[index].push(shoes);
          index += 1;
          if (index > 2) {
            index = 0;
          }
        }
      }
      setShoesColumns(columns);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="container">
      <div className="row">
        <div className="col d-flex justify-content-center">
          <div className="card text-center shadow-light mb-3">
            <h2 className="card-title">Shoes</h2>
              <a className="btn-outline-secondary" href="/shoes/new/" role="button">Add Shoe</a>
            </div>
          </div>
        </div>
      <div className="row">
        {shoesColumns.map((shoesColumnList, index) => {
          return <ShoesColumn key={index} list={shoesColumnList} />;
        })}
      </div>
    </div>
  );
};

export default ShoesList;
