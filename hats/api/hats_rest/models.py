from django.db import models


# Create your models here.
class LocationVO(models.Model):

    name = models.CharField(max_length=200)
    import_href = models.CharField(max_length=200, unique=True)

    def __str__(self):
       return self.name



class Hats(models.Model):
   location = models.ForeignKey(
       LocationVO,
       related_name='hats',
       on_delete=models.CASCADE,
   )
   fabric = models.CharField(max_length=100)
   style_name = models.CharField(max_length=100)
   color = models.CharField(max_length=100)
   picture_url = models.URLField()

   def __str__(self):
       return f'{self.style_name}, {self.color}, at {self.location.name}'
