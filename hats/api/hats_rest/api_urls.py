from django.urls import path
from .api_views import api_hat_list, api_show_hat_detail


urlpatterns = [
   path("hats/", api_hat_list, name="api_hat_list"),
   path("hats/<int:pk>/", api_show_hat_detail, name="api_show_hat_detail"),
]
