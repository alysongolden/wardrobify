import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import LocationVO, Hats


# Create your views here.
class LocationVOEncoder(ModelEncoder):
   model = LocationVO
   properties = [
       'import_href',
       'name',
       'id',
   ]


class HatsListEncoder(ModelEncoder):
   model = Hats
   properties = [
       'id',
       'fabric',
       'style_name',
       'color',
       'picture_url',
       'location',
   ]
   encoders = {"location": LocationVOEncoder()}





@require_http_methods(['GET', 'POST'])
def api_hat_list(request):
   if request.method == 'GET':
       hats = Hats.objects.all()
       return JsonResponse({'hats': hats}, encoder=HatsListEncoder, safe=False)
   elif request.method == 'POST':
       content = json.loads(request.body)
       if "location" not in content:
           return JsonResponse(
               {"message": "Missing 'location' in request body"}, status=400
           )

       try:
           location = LocationVO.objects.get(import_href=content['location'])
           content['location'] = location
       except LocationVO.DoesNotExist:
           return JsonResponse({"message": "Invalid location id"}, status=400)


       hat = Hats.objects.create(**content)
       return JsonResponse({"hat": hat}, encoder=HatsListEncoder, safe=False)


@require_http_methods(['GET', 'PUT', 'DELETE'])
def api_show_hat_detail(request, id):
   if request.method == 'DELETE':
       count,_ = Hats.objects.filter(id=id).delete()
       return JsonResponse({'deleted':count>0})
   elif request.method == 'GET':
       hat = Hats.objects.get(id=id)
       return JsonResponse(
           hat,
           encoder=HatsListEncoder,
           safe=False,
       )
   else:
       content = json.loads(request.body)
       try:
           if 'location' in content:
               location = LocationVO.objects.get(id=content['location'])
               content['location'] = location
       except LocationVO.DoesNotExist:
           return JsonResponse({'message': 'No location found'})
       Hats.objects.filter(id=id).update(**content)
       hat = Hats.objects.get(id=id)
       return JsonResponse(
           hat,
           encoder=HatsListEncoder,
           safe=False,
       )
